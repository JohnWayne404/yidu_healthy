<?php

return [
    'Name'  =>  '课程名称',
    'Desc'  =>  '课程介绍',
    'Price'  =>  '价格',
    'Cover'  => '课程封面',
    'Teacher_qrcode_img'  =>  '老师二维码',
    'Create_time'  =>  '创建时间',
    'Update_time'  =>  '更新时间'
];
