<?php

return [
    'Lesson_id'  =>  '课程ID',
    'Title'  =>  '章节名称',
    'Content'  =>  '章节内容',
    'Paraphrase'  =>  '本章释义',
    'Create_time'  =>  '创建时间',
    'Update_time'  =>  '更新时间'
];
