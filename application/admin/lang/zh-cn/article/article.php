<?php

return [
    'Title'  =>  '标题',
    'Image'  =>  '封面',
    'Description'  =>  '简介',
    'Content'  =>  '内容',
    'Create_time'  =>  '创建时间',
    'Update_time'  =>  '更新时间'
];
