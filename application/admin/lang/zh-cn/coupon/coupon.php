<?php

return [
    'Code'  =>  '激活码',
    'Price'  =>  '金额',
    'Uid'  =>  '使用人',
    'Is_use'  =>  '是否使用',
    'Create_time'  =>  '创建时间',
    'Use_time'  =>  '使用时间'
];
