<?php

return [
    'Title'  =>  '标题',
    'Img'  =>  '图片',
    'Url'  =>  '链接',
    'Sort'  =>  '排序',
    'Create_time'  =>  '创建时间',
    'Update_time'  =>  '更新时间'
];
