<?php

return [
    'Name'  =>  '商品名称',
    'Category_id' => '商品分类',
    'Cover'  =>  '商品封面',
    'Banner'  =>  '商品图片',
    'Price'  =>  '商品价格',
    'Stock'  =>  '库存',
    'Content'  =>  '商品详情',
    'Status'  =>  '状态',
    'Create_time'  =>  '创建时间',
    'Update_time'  =>  '更新时间'
];
