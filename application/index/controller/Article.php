<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\model\Article as ArticleModel;

class Article extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function show($id)
    {
        $info = ArticleModel::get($id);
        if(!$info) {
            $this->error('文章不存在');
        }
        $this->assign('info', $info);
        $this->assign('title', $info['title']);
        return $this->view->fetch();
    }
}
