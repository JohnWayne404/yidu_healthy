<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;

class Lesson extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
        $this->user_mobile = Db::name('user')->where(['id' => $this->auth->id])->value('mobile');
    }

    public function index()
    {
        $order = Db::name('order')->where(['user_id' => $this->auth->id, 'type' => 1, 'status' => 1])->find();
        if($order) {
            $this->redirect('lesson/today');
        }
        $lesson = Db::name('lesson')->limit(20)->select();
        $this->assign('lesson', $lesson);
        $this->assign('title', '课程介绍');
        return $this->view->fetch();
    }

    public function plan() {
        // if(!$this->user_mobile) {
        //     $this->redirect('user/archives');
        // }
        $date = [
            'unconmpleteDate' => ['2018-11-14', '2018-11-15', '2018-11-16', '2018-11-17', '2018-11-18', '2018-11-19', '2018-11-20', '2018-11-21', '2018-11-22', '2018-11-23', '2018-11-24', '2018-11-25', '2018-11-26', '2018-11-27', '2018-11-28', '2018-11-29', '2018-11-30', '2018-12-01'],
            'readDate' => ['2018-11-12', '2018-11-13'],
            'unReadDate' => ['2018-11-11'],
        ];
        $this->assign('date', $date);
        $this->assign('title', '学习计划');
        return $this->view->fetch();
    }

    public function today()
    {
        $uid = $this->auth->id;
        $order = Db::name('order')->where(['user_id' => $uid, 'status' => 1, 'type' => 1])->find();
        if(!$order) {
            $this->redirect('lesson/index');
        }
        $pay_time = $order['pay_time'];
        $diff_day = round((time() - $pay_time) / 3600 / 24);
        $course = Db::name('course')->where(['lesson_id' => $order['goods_id']])->field('id,title,lesson_id')->select();
        // var_dump(date('Y-m-d',$pay_time));
        // halt($diff_day);
        $today_course = isset($course[$diff_day - 1]) ? $course[$diff_day - 1] : [];
        // halt($today_course);
        // if(!$order['status']) {
        //     $this->redirect('lesson/index');
        // }
        // if(!$this->user_mobile) {
        //     $this->redirect('user/archives');
        // }
        $this->assign('today_course', $today_course);
        $this->assign('title', '今日阅读');
        return $this->view->fetch();
    }

}
