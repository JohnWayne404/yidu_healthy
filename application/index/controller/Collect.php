<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;

class Collect extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        $this->assign('title', '我的收藏');
        return $this->view->fetch();
    }

    /**
     * 收藏
     * @return [type] [description]
     */
    public function collect($goods_id) {
        if(request()->isAjax()) {
            $uid = $this->auth->id;
            $collect = Db::name('collect')->where(['uid' => $uid, 'goods_id' => $goods_id])->find();
            if($collect) {
                $this->success('已收藏');
            }else{
                $result = Db::name('collect')->insert(['uid' => $uid, 'goods_id' => $goods_id]);
                if($result) {
                    $this->success('收藏成功');
                }else{
                    $this->error('收藏失败');
                }
            }
        }
    }

    /**
     * 取消收藏
     * @return [type] [description]
     */
    public function cancel($goods_id)
    {
        if(request()->isAjax()) {
            $uid = $this->auth->id;
            $collect = Db::name('collect')->where(['uid' => $uid, 'goods_id' => $goods_id])->delete();
            if($collect) {
                $this->success('取消收藏成功');
            }else{
                $this->error('取消收藏失败');
            }
        }
    }
}
