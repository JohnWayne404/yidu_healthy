<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;

class Goods extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        $keywords = $this->request->param('keywords');
        $where = [];
        if(isset($keywords)) {
            $where['name'] = ['like', '%'. $keywords . '%'];
        }
        $list = Db::name('goods')
            ->field('id,name,cover,status,price')
            ->where($where)
            ->where('status',1)
            ->paginate(10);
        $this->assign('list', $list);
        $this->assign('title', '商品列表');
        return $this->view->fetch();
    }

    public function detail($id)
    {
        $goods = Db::name('goods')->where(['id' => $id, 'status' => 1])->find();
        if(!$goods) {
            $this->error('该商品不存在或已下架');
        }
        $collect = Db::name('collect')->where(['uid' => $this->auth->id, 'goods_id' => $id])->find();
        $goods['banner'] = explode(',', $goods['banner']);
        $this->assign('goods', $goods);
        $this->assign('collect', $collect);
        $this->assign('title', '商品详情');
        return $this->view->fetch();
    }
}
