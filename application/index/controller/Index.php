<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use think\Db;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        // $notice = $this->app->notice;
        // $userId = $this->user['openid'];
        // $templateId = 'EKSpWOvWR26fKRKXO7BnOK85_h6SAHynF8RBcTmyMks';
        // $url = 'http://overtrue.me';
        // $data = array(
        //          "first"  => "恭喜你购买成功！",
        //          "keyword1"   => "巧克力",
        //          "keyword2"   => "20180805",
        //          "remark" => "非常感谢您的购买，欢迎下次惠顾",
        //         );
        //
        // $result = $notice->uses($templateId)->withUrl($url)->andData($data)->andReceiver($userId)->send();
        // halt($result);
        $banner = Db::name('banner')->order('sort asc')->limit(5)->select();
        $goods = Db::name('goods')->field('id,name,cover,price,status')->where(['status' => 1])->limit(10)->select();
        $this->assign('banner', $banner);
        $this->assign('goods', $goods);
        $this->assign('title', '邑度营养商城');
        return $this->view->fetch();
    }

    public function test()
    {
        halt(generate_code());
    }


}
