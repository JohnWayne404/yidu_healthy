<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;

class Category extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        $category = Db::name('category')->where(['type' => 'goods', 'status' => 'normal'])->select();
        $this->assign('category', $category);
        return $this->view->fetch();
    }

    public function goods()
    {
        $category_id = $this->request->param('category_id');
        $where = [];
        $where['category_id'] = $category_id;
        $list = Db::name('goods')
            ->field('id,name,cover,status,price')
            ->where($where)
            ->where('status',1)
            ->paginate(10);
        return $list;
    }
}
