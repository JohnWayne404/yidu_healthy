<?php

namespace app\common\model;

use think\Model;

/**
 * 问题
 */
class Question Extends Model
{
    // 表名
    protected $name = 'question';

    public function options()
    {
        return $this->hasMany('options', 'question_id');
    }

}
