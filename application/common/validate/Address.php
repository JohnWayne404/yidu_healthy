<?php

namespace app\common\validate;

use think\Validate;

class Address extends Validate
{

    /**
     * 验证规则
     */
    protected $rule = [
        'consignee' => 'require|max:50',
        'mobile' => 'require|max:11',
        'area' => 'require',
        'address' => 'require',
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'consignee' => '收货人不能为空',
        'mobile' => '手机号不能为空',
        'area' => '所在地区不能为空',
        'address' => '详细地址不能为空',
    ];
}
