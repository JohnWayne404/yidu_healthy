define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'lesson/question/index',
                    add_url: 'lesson/question/add',
                    edit_url: 'lesson/question/edit',
                    del_url: 'lesson/question/del',
                    multi_url: 'lesson/question/multi',
                    table: 'question',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("click", ".fieldlist .append-option", function () {
                    var rel = parseInt($(this).closest("dl").attr("rel")) + 1;
                    $(this).closest("dl").attr("rel", rel);
                    $('<dd>' +
                        '<input type="text" name="row[field]['+ rel +']" class="form-control" id="field-0" value="" size="40"> ' +
                        ' <span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span>' +
                    '</dd>').insertBefore($(this).parent());
                });
            }
        }
    };
    return Controller;
});
