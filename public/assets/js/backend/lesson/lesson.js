define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'lesson/lesson/index',
                    add_url: 'lesson/lesson/add',
                    edit_url: 'lesson/lesson/edit',
                    del_url: 'lesson/lesson/del',
                    multi_url: 'lesson/lesson/multi',
                    table: 'lesson',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'cover', title: __('Cover'), formatter:Table.api.formatter.image},
                        {field: 'teacher_qrcode_img', title: __('Teacher_qrcode_img'), formatter:Table.api.formatter.image},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        // {field: 'operate', title: __('Operate'), table: table, buttons: [
                        //        {name: 'question', text: '', title: '课程问题', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'lesson/question/index/ids/{ids}'},
                        //    ], events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
